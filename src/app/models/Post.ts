export class Post{
    public id:number;
    public user:number;
    public category:number;
    public title:string;
    public content:string;
    public image:string;
    public createdAt:any;
    public updatedAt:any;

    constructor(id,userId,categoryId,title,content,image,createdAt,updatedAt){
        this.id=id;
        this.user=userId;
        this.category=categoryId;
        this.title=title;
        this.content=content;
        this.image=image;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
    }
}