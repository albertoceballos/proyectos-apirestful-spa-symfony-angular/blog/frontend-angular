export class User {
    public id: number;
    public name: string;
    public surname: string;
    public role: string;
    public email: string;
    public password: string;
    public description: string;
    public image: string;
    public createdAt: any;
    public updatedAt: any;

    constructor(id, name, surname, role, email, password, description, image, createdAt, updatedAt) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.role = role;
        this.email = email;
        this.password = password;
        this.description = description;
        this.image=image;
        this.createdAt = createdAt;
        this.updatedAt = updatedAt;
    }

}