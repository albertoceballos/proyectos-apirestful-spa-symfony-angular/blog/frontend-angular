export class Category{
 
    public id:number;
    public name:string;
    public createdAt:any;
    public updatedAt:any;

    constructor(id,name,createdAt,updatedAt){
        this.id=id;
        this.name=name;
        this.createdAt=createdAt;
        this.updatedAt=updatedAt;
    }
    
}