import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { GLOBAL } from './global';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  public url;
  constructor(private _httpClient:HttpClient) {
    this.url=GLOBAL.url;
   }

   add(token,post):Observable<any>{
     post.content=GLOBAL.htmlEntities(post.content);
     let json=JSON.stringify(post);
     let params="json="+json;
     let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization',token);

     return this._httpClient.post(this.url+'post/new',params,{headers:headers});
   }

   //todos los posts
   getAllPosts():Observable<any>{

    return this._httpClient.get(this.url+'posts');
   }

   //detalle del post
   getDetailPost(postId):Observable<any>{

    return this._httpClient.get(this.url+'post/'+postId);
   }

   //actualizar un post
   updatePost(token,post,id):Observable<any>{
    post.content=GLOBAL.htmlEntities(post.content);
    let json=JSON.stringify(post);
    let params="json="+json;
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization',token);

    return this._httpClient.put(this.url+'post/update/'+id,params,{headers:headers});
   }

   //borrar un post
   deletePost(token,id):Observable<any>{
    let headers=new HttpHeaders().set('Authorization',token);

    return this._httpClient.delete(this.url+'post/delete/'+id,{headers:headers});
   }

   //posts de un usuario por su id
   postsByUser(id):Observable<any>{
     let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
     return this._httpClient.get(this.url+'posts/user/'+id,{headers:headers});
   }

   //buscar un post por su título o contenido
   searchPosts(searchString):Observable<any>{
     let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
     return this._httpClient.get(this.url+'posts/search/'+ searchString,{headers:headers});
   }
}
