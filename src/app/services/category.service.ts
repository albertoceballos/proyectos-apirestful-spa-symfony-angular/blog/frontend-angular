import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
//modelos
import { Category } from '../models/Category';
//url global de la api
import { GLOBAL } from '../services/global';


@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  public url;
  constructor(private _httpClient: HttpClient) {
    this.url = GLOBAL.url;
  }

  addCategory(token, category: Category): Observable<any> {
    let json = JSON.stringify(category);
    let params = "json=" + json;
    let headers = new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded').set('Authorization', token);

    return this._httpClient.post(this.url + 'category/add', params, { headers: headers });
  }

  //devolver todas las categorías
  getCategories(): Observable<any> {
    return this._httpClient.get(this.url+'categories');
  }

  detailCategory(id):Observable<any>{
    return this._httpClient.get(this.url+'category/'+id);
  }
}
