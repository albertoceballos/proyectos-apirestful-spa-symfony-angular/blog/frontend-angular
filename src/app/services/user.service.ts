import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable} from 'rxjs';

//url global
import { GLOBAL } from './global';

import {User} from '../models/User';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  public url:string;


  constructor(public _httpClient: HttpClient) {
    this.url = GLOBAL.url;
  }

  register(user:User):Observable<any> {
    let json = JSON.stringify(user);
    let params = "json=" + json;
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    
    return this._httpClient.post(this.url+'register',params,{headers:headers});

  }

  login(user,getToken=null):Observable<any>{
    if(getToken!=null){
      user.getToken='true';
    }
    let json=JSON.stringify(user);
    let params="json="+json;
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');

    return this._httpClient.post(this.url+'login',params,{headers:headers});
  }

  userUpdate(token,user):Observable<any>{
    user.description=GLOBAL.htmlEntities(user.description);
    let json=JSON.stringify(user);
    let params="json="+json;
    let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded').set('Authorization',token);
  
    return this._httpClient.put(this.url+'user/update',params,{headers:headers});
  }

  //sacar detalles del usuario por su id
  userDetail(id):Observable<any>{
   let headers=new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded')
   return this._httpClient.get(this.url+'user/detail/'+id,{headers:headers}); 
  }

  //sacar identidad desde el localStorage
  getIdentity(){
    let identity=JSON.parse(localStorage.getItem('identity'));
    return identity;
  }

  //Sacar token desde el localStorage
  getToken(){
    let token=localStorage.getItem('token');
    return token;
  }
}
