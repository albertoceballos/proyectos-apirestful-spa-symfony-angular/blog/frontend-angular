import { Component, OnInit, DoCheck } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from './services/user.service';
import { CategoryService } from './services/category.service';
import { PostService } from './services/post.service';

import { GLOBAL } from './services/global';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [UserService, CategoryService, PostService]
})
export class AppComponent implements DoCheck, OnInit {
  public title = 'blog-symfony-angular';
  public identity;
  public token;
  public url: string;
  public categories;
  public busqueda: string;
  public searchStatus: string;

  constructor(private _userService: UserService,
    private _categoryService: CategoryService,
    private _postservice: PostService,
    private _router: Router) {
    this.url = GLOBAL.url;
  }

  ngDoCheck() {
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {
    this.getCategories();
  }

  //recibe categorias
  getCategories() {
    this._categoryService.getCategories().subscribe(
      response => {
        if (response.status == 'success') {
          this.categories = response.categories;
          console.log(this.categories);
        }
      },
      error => {
        console.log(error);
      }
    );
  }

  logout() {
    this.identity = null;
    this.token = null;
    localStorage.clear();
    this._router.navigate(['/login']);

  }

  search() {
    this._router.navigate(['/search',this.busqueda]);
  }
}
