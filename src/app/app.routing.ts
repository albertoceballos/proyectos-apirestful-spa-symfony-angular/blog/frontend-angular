import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

//components
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UserEditComponent } from './components/user-edit/user-edit.component';
import { CategoryNewComponent } from './components/category-new/category-new.component';
import { PostNewComponent } from './components/post-new/post-new.component';
import { PostDetailComponent } from './components/post-detail/post-detail.component';
import { PostEditComponent } from './components/post-edit/post-edit.component';
import { CategoryDetailComponent } from './components/category-detail/category-detail.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SearchComponent } from './components/search/search.component';
//guard
import { IdentityGuard } from './services/identity.guard';



const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'login', component: LoginComponent },
    { path: 'registro', component: RegisterComponent },
    { path: 'user-edit', component: UserEditComponent, canActivate: [IdentityGuard] },
    { path: 'category/new', component: CategoryNewComponent, canActivate: [IdentityGuard] },
    { path: 'post/new', component: PostNewComponent, canActivate: [IdentityGuard] },
    { path: 'post/:id', component: PostDetailComponent },
    { path: 'post/edit/:id', component: PostEditComponent, canActivate: [IdentityGuard] },
    { path: 'category/:id', component: CategoryDetailComponent },
    { path: 'profile/:id', component: ProfileComponent },
    { path: 'search/:string', component: SearchComponent },
    { path: '**', component: LoginComponent },
];

export const appRoutingProviders: any[] = [];
export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);

