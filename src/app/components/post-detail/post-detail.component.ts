import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Router,Params} from '@angular/router';

//servicio
import {PostService} from '../../services/post.service';
import {UserService} from '../../services/user.service';

//modelo
import {Post} from '../../models/Post';

import {GLOBAL} from '../../services/global';


@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
  providers: [PostService,UserService],
})
export class PostDetailComponent implements OnInit {
  public pageTitle:string;
  public post:Post;
  public url:string;
  public token;
  public identity;
  public postId:number;
  public message:string;

  constructor(private _activatedRoute:ActivatedRoute, private _router:Router, private _postService:PostService, private _userService:UserService) {
    this.url=GLOBAL.url;
    this.token=this._userService.getToken();
    this.identity=this._userService.getIdentity();
   }

  ngOnInit() {
    //conseguimos el parámtro del id del post que viene por la URL
    this._activatedRoute.params.subscribe(
      params=>{
        let postId=params['id'];
        this.postId=postId;
        this._postService.getDetailPost(postId).subscribe(
          response=>{
            if(response.status=='success'){
              console.log(response);
              this.pageTitle=response.post.title;
              this.post=response.post;
            }
          },
          error=>{
            console.log(error);
          }
        );
      }
    );
  }

  delete(){
    this._postService.deletePost(this.token,this.postId).subscribe(
      response=>{
        if(response.status=="success"){
          this.message=response.message;
          alert(this.message);
          this._router.navigate(['/']);
        }else{
          this.message='No se pudo borrar la publicación';
          alert(this.message);
        }
      },
      error=>{
        console.log(error);
      }
    );
  }

}
