import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

//sevicios
import { CategoryService } from '../../services/category.service';
import { UserService } from '../../services/user.service';


//modelos
import { Category } from '../../models/Category';

import {GLOBAL} from '../../services/global';



@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css'],
  providers: [CategoryService,UserService],
})
export class CategoryDetailComponent implements OnInit {
  public pageTitle;
  public category;
  public posts:Array<any>;
  public url
  public identity;
  constructor(private _categoryService: CategoryService, private _activatedRoute: ActivatedRoute,private _userService:UserService) {
    this.url=GLOBAL.url;
    this.identity=this._userService.getIdentity();
   }

  ngOnInit() {
    this.getCategory();
  }

  getCategory() {
    this._activatedRoute.params.subscribe(
      params => {
        let catId = params['id'];
        this._categoryService.detailCategory(catId).subscribe(
          response=>{
            if(response.status=='success'){
              console.log(response.category);
              this.category=response.category;
              this.pageTitle=this.category.name;
              this.posts=this.category.posts;
            }
          }
        ),
        error=>{
          console.log(error);
        }
      }
    );
  }

}
