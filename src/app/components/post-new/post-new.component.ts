import { Component, OnInit } from '@angular/core';

import { UserService } from '../../services/user.service';
import { CategoryService } from '../../services/category.service';
import { PostService } from '../../services/post.service';

//url global API
import { GLOBAL } from '../../services/global';

//modelos
import { User } from '../../models/User';
import { Post } from '../../models/Post';

@Component({
  selector: 'app-post-new',
  templateUrl: './post-new.component.html',
  styleUrls: ['./post-new.component.css'],
  providers: [UserService, CategoryService, PostService]
})
export class PostNewComponent implements OnInit {

  public pageTitle;
  public identity;
  public token;
  public post: Post;
  public categories;
  public status: string;
  public message:string;
  public froala_options: Object = {
    charCounterCount: true,
    language: 'es',
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
  };

  public afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.gif,.jpeg",
    maxSize: "20",
    uploadAPI: {
      url: GLOBAL.url + 'post/uploadImage',
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: true,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube una imagen...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !',
    }
  };

  constructor(private _userService: UserService, private _categoryService: CategoryService, private _postService: PostService) {
    this.pageTitle = "Nueva entrada";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();



  }

  ngOnInit() {
    this.post = new Post('','', '', '', '', '', '', '');
    //console.log(this.post);
    console.log(this.categories);

    this._categoryService.getCategories().subscribe(
      response => {
        this.categories = response.categories;
        console.log(this.categories);
      },
      error => {
        console.log(error);
      }
    );
  }

  imageUpload(datos) {
    let data = JSON.parse(datos.response);
    this.post.image = data.file;

  }

  onSubmit() {
    console.log(this.post);
    this._postService.add(this.token, this.post).subscribe(
      response => {
        if (response.status == 'success') {
          this.status = 'success';
        } else {
          this.status = 'error';
        }
        this.message=response.message;
      },
      error => {
        console.log(error);
      }
    );
  }

}
