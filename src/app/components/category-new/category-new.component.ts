import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';

//modelos
import { Category } from '../../models/Category';
import { User } from '../../models/User';

//servicios
import { UserService } from '../../services/user.service';
import { CategoryService } from '../../services/category.service';

@Component({
  selector: 'app-category-new',
  templateUrl: './category-new.component.html',
  styleUrls: ['./category-new.component.css'],
  providers: [UserService,CategoryService]
})
export class CategoryNewComponent implements OnInit {
  public pageTitle;
  public token;
  public category;
  public message;
  public status;
  constructor(private _userService: UserService,private _categoryService:CategoryService) {
    this.pageTitle = "Nueva categoría";
    this.token = _userService.getToken();
    this.category = new Category('', '', '', '');
  }

  ngOnInit() {

  }

  onSubmit() {
    this._categoryService.addCategory(this.token,this.category).subscribe(
      response=>{
       this.status=response.status;
       this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status="error";
        this.message="Error en el servidor, inténtalo de nuevo";
      }
    );
  }

}
