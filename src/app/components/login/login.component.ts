import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';

//modelo
import {User} from '../../models/User';

//servicio
import {UserService} from '../../services/user.service';
import { from } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers:[UserService]
})
export class LoginComponent implements OnInit {

  public pageTitle;
  public user:User;
  public identity;
  public token:string;
  public status:string;
  public message:string;
  constructor(private _userService:UserService, private _router:Router) {
    this.pageTitle="Login";
    this.user=new User('','','','','','','','','','');
   }

  ngOnInit() {
  }

  onSubmit(){
    this._userService.login(this.user).subscribe(
      response=>{
        if(response.status=='success'){
          this.identity=response.data;
          console.log(this.identity);
          this._userService.login(this.user,true).subscribe(
            response=>{
              if(response.status="success"){
                this.status='success';
                this.token=response.data;
                localStorage.setItem('identity',JSON.stringify(this.identity));
                localStorage.setItem('token',this.token);
                setTimeout(()=>{
                  this._router.navigate(['/']);
                },2500);
              }else{
                this.status='error';
              }          
            },
            error=>{
              console.log(error);
              this.status='error';
            }
          );
        }else{
          this.status="error";
        }
        this.message=response.message;
      },
      error=>{
        console.log(error);
        this.status='error';
      }
    );

  }

}
