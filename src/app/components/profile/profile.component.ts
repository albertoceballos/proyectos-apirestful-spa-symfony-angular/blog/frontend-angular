import { Component, OnInit, DoCheck } from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';

//servicios
import {PostService} from '../../services/post.service';
import {UserService} from '../../services/user.service';

//url global API
import {GLOBAL} from '../../services/global';

//modelos
import {Post} from '../../models/Post';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  providers:[PostService,UserService],
})
export class ProfileComponent implements OnInit {
  public pageTitle:string;
  public url:string;
  public user;
  public posts:Array<Post>;
  pulic 
  constructor(private _router:Router,
    private _activatedRoute:ActivatedRoute,
     private _userService:UserService,
     private _postService:PostService) {
    this.pageTitle="Perfil de ";
    this.url=GLOBAL.url;
   }

  ngOnInit() {
    this.load();
  }

  load(){
    //extraer el id de la URL
    this._activatedRoute.params.subscribe(
      params=>{
        let userId=params['id'];
        //llamar al sevicio para llamar al método que extrae los detalles del usuario 
        this._userService.userDetail(userId).subscribe(
          response=>{
            if(response.status=='success'){
              this.user=response.user;
              //título de la página
              this.pageTitle=this.pageTitle+this.user.name+" "+this.user.surname;
              //llamar al método del servicio para sacar los posts del usuario
              this._postService.postsByUser(userId).subscribe(
                response=>{
                  if(response.status=="success"){
                    this.posts=response.posts;
                    console.log(this.posts);
                  }else{
                    this.posts=[];
                  }
                },
                error=>{
                  console.log(error);
                }
              );
            }
          },
          error=>{
            console.log(error);
          }
        );
      }
    );
  }

}
