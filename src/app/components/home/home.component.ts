import { Component, OnInit } from '@angular/core';
import {Router, ActivatedRoute,Params} from '@angular/router';

//modelo
import {Post} from '../../models/Post';

//servicios
import {PostService} from '../../services/post.service';
import {UserService} from '../../services/user.service';

import {GLOBAL} from '../../services/global';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers:[PostService,UserService],
})
export class HomeComponent implements OnInit {
  public pageTitle;
  public posts:Array<Post>;
  public url;
  public identity;
  public token;
  public message:string;

  constructor(private _postService:PostService,
     private _userService:UserService,
     private _router:Router,) {

    this.pageTitle="Inicio";
    this.url=GLOBAL.url;
    this.identity=this._userService.getIdentity();
    this.token=this._userService.getToken();
   }

  ngOnInit() {
    this.getPosts();
  }

  getPosts(){
    this._postService.getAllPosts().subscribe(
      response=>{
        if(response.status=="success"){
          this.posts=response.posts;
          console.log(this.posts);
        }
        
      },
      error=>{
        console.log(error);
      }
    );
  }




//borrar posts
  delete(id){
    console.log(id);
    this._postService.deletePost(this.token,id).subscribe(
      response=>{
        if(response.status=='success'){
          this.message=response.message;
        }else{
          this.message='No se pudo borrar la publicación';
        }
        alert(this.message);
        this.getPosts();
      },
      error=>{
        console.log(error);
      }
    );

  }
}
