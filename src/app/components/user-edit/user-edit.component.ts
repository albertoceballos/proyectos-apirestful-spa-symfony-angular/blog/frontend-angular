import { Component, OnInit } from '@angular/core';

import { User } from '../../models/User';
//url global
import {GLOBAL} from '../../services/global';
import { UserService } from '../../services/user.service';
//Jquery
import * as $ from 'jquery';

@Component({
  selector: 'app-user-edit',
  templateUrl: './user-edit.component.html',
  styleUrls: ['./user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  public pageTitle: string;
  public identity;
  public token: string;
  public user: User;
  public status: string;
  public message:string;
  public url;
  public froala_options: Object = {
    charCounterCount: true,
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat','emoticons'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat','emoticons'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat','emoticons'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat','emoticons'],
  };

  afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.gif,.jpeg",
    maxSize: "20",
    uploadAPI:  {
      url:GLOBAL.url+'uploadAvatar',
      headers: {
     "Authorization" : this._userService.getToken(),
      }
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: true,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube un avatar...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !',
    }
};

  constructor(private _userService: UserService) {
    this.pageTitle = "Ajustes de Usuario";
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
    this.url=GLOBAL.url;
    this.user = new User(this.identity.sub,
      this.identity.name,
      this.identity.surname,
      'ROLE_USER',
      this.identity.email,
      '',
      this.identity.description,
      this.identity.avatar,
      this.identity.createdAt,
      this.identity.updatedAt);
  }

  ngOnInit() {
    console.log(this.user)
    console.log(this.token);
  }

  onSubmit() {
    this._userService.userUpdate(this.token, this.user).subscribe(
      response => {
        this.status=response.status;
        this.message=response.message;
        localStorage.setItem('identity',JSON.stringify(this.user));
        setTimeout(()=>{
          $('#Message').fadeOut(1000,()=>{
            this.status=null;
          });
        },4000);
      },
      error => {
        console.log(error);
        this.status = "error";
      }
    );
  }

  avatarUpload(datos){
    let data=JSON.parse(datos.response);
    this.user.image=data.user.image;
    console.log(this.user.image);
    this.identity.image=this.user.image;
  }

}
