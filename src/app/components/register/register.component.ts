import { Component, OnInit } from '@angular/core';

//modelo
import {User} from '../../models/User';
//servicios
import {UserService} from '../../services/user.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers:[UserService]
})
export class RegisterComponent implements OnInit {
  public pageTitle;
  public user:User;
  public status:string;
  
  constructor(private _userService:UserService) {
    this.pageTitle="Regístrate";
    this.user=new User('','','','ROLE_USER','','','','','','');

   }

  ngOnInit() {

  }

  onSubmit(){
    this._userService.register(this.user).subscribe(
      response=>{
        console.log(response);
        this.status="success";
      },
      error=>{
        console.log(error);
        this.status="error";

      }
    );
  }

}
