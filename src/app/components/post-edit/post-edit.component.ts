import { Component, OnInit } from '@angular/core';
import {ActivatedRoute,Params,Router} from '@angular/router';
//servicios
import { UserService } from '../../services/user.service';
import { PostService } from '../../services/post.service';
import { CategoryService } from '../../services/category.service';
//url global api
import {GLOBAL} from '../../services/global';
//modelos
import {Post} from '../../models/Post';
import { Category } from '../../models/Category';


@Component({
  selector: 'app-post-edit',
  templateUrl: './post-edit.component.html',
  styleUrls: ['./post-edit.component.css'],
  providers: [UserService,PostService],
})
export class PostEditComponent implements OnInit {
  public pageTitle;
  public post;
  public token;
  public identity;
  public url;
  public categories:Array<Category>;
  public status: string;
  public message:string;
  public froala_options: Object = {
    charCounterCount: true,
    language: 'es',
    toolbarButtons: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
    toolbarButtonsXS: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
    toolbarButtonsSM: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
    toolbarButtonsMD: ['bold', 'italic', 'underline', 'paragraphFormat', 'emoticons'],
  };

  public afuConfig = {
    multiple: false,
    formatsAllowed: ".jpg,.png,.gif,.jpeg",
    maxSize: "20",
    uploadAPI: {
      url: GLOBAL.url + 'post/uploadImage',
    },
    theme: "attachPin",
    hideProgressBar: true,
    hideResetBtn: true,
    hideSelectBtn: true,
    replaceTexts: {
      selectFileBtn: 'Select Files',
      resetBtn: 'Reset',
      uploadBtn: 'Upload',
      dragNDropBox: 'Drag N Drop',
      attachPinBtn: 'Sube una imagen...',
      afterUploadMsg_success: 'Successfully Uploaded !',
      afterUploadMsg_error: 'Upload Failed !',
    }
  };
  constructor(private _userService:UserService, private _postService:PostService, private _categoryService:CategoryService , private _activatedRoute:ActivatedRoute) {
    this.pageTitle = "Editar publicación";
    this.token=this._userService.getToken();
    this.identity=this._userService.getIdentity();
    this.url=GLOBAL.url;
    
  }

  ngOnInit() {
    this.post=new Post('',this.identity.sub,'','','','','','');
    //cargo categorías
    this._categoryService.getCategories().subscribe(
      response => {
        this.categories = response.categories;
        console.log(this.categories);
      },
      error => {
        console.log(error);
      });

    //scao el Id del post y llamo al servicio para sacar los detalles del post con ese Id  
    this._activatedRoute.params.subscribe(
      params=>{
        let id=params['id'];
        this._postService.getDetailPost(id).subscribe(
          response=>{
            if(response.status="success"){
              this.post=response.post;
              this.post.category=this.post.category.id;
              console.log(response.post);
            }
          },
          error=>{
            console.log(error);
          }
        );
      }
    );
    
  }

  imageUpload(datos){
    let data=datos.response;
    this.post.image=data.file;

  }

  onSubmit(){
    console.log(this.post);
    this._postService.updatePost(this.token,this.post,this.post.id).subscribe(
      response=>{
        console.log(response);
      },
      error=>{
        console.log(error);
      }
    );
  }

}
