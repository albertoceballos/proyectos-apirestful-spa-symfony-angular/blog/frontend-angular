import { Component, OnInit } from '@angular/core';
import {Router,ActivatedRoute,Params} from '@angular/router';

import {GLOBAL} from '../../services/global';
//servicios
import {PostService} from '../../services/post.service';
//modelos
import {Post} from '../../models/Post';


@Component({
  selector: 'app-search',
  templateUrl: '../home/home.component.html',
  styleUrls: ['./search.component.css'],
  providers: [PostService]
})
export class SearchComponent implements OnInit {

  public url:string;
  public pageTitle:string;
  public searchStatus:string;
  public posts:Array<Post>;

  constructor(private _postService:PostService,
     private _router:Router,
     private _activatedRoute:ActivatedRoute) {
    this.url=GLOBAL.url;
    this.pageTitle="Resultados de la búsqueda:"
   }

  ngOnInit() {
    //extraer el string de búsqueda de la URL
    this._activatedRoute.params.subscribe(
      params=>{
        let searchString=params['string'];
        //llamar al servicio para realizar la búsqueda
        this._postService.searchPosts(searchString).subscribe(
          response=>{
            if(response.status=='success'){
              this.searchStatus='success';
              this.posts=response.posts;
            }else{
              this.posts=[];
              this.searchStatus='error';
            }
          },
          error=>{
            console.log(error);
            this.searchStatus='error';
          }
        );

      }
    );
  }

}
